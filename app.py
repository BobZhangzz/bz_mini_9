import streamlit as st
from transformers import pipeline

st.title("Simple Language Model Demo")

# Load the language model pipeline
model = pipeline("text-generation", model="gpt2")

# Text generation form
prompt = st.text_input("Enter text prompt", "Once upon a time")
max_length = st.slider(
    "Maximum length of generated text", min_value=10, max_value=100, value=50, step=10
)

# Generate text on button click
if st.button("Generate Text"):
    generated_text = model(prompt, max_length=max_length, num_return_sequences=1)[0][
        "generated_text"
    ]
    st.text_area("Generated Text", generated_text, height=200)
