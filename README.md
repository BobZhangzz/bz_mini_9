# bz_mini_9
In this project, we are going to build a website which connects to a huggingface open source large language model. We will be able to input text into a text prompt and the model will generate text based on the input. We will be using the GPT-2 model from huggingface. The website will be built using streamlit and the model will be called using the transformers library from huggingface. The website will be deployed using AWS EC2. The website will be hosted at the following link: 

## Project Structure
- `app.py`: The main file which contains the streamlit code for the website.
```python
import streamlit as st
from transformers import pipeline

st.title("Simple Language Model Demo")

# Load the language model pipeline
model = pipeline("text-generation", model="gpt2")

# Text generation form
prompt = st.text_input("Enter text prompt", "Once upon a time")
max_length = st.slider(
    "Maximum length of generated text", min_value=10, max_value=100, value=50, step=10
)

# Generate text on button click
if st.button("Generate Text"):
    generated_text = model(prompt, max_length=max_length, num_return_sequences=1)[0][
        "generated_text"
    ]
    st.text_area("Generated Text", generated_text, height=200)
```

- `requirements.txt`: The file which contains the required python packages for the project.
```md
#devops
black==22.3.0
click==8.1.3 
pytest==7.1.3
pytest-cov==4.0.0
pylint==2.15.3
boto3==1.24.87
#web
transformers==4.39.3
tf-keras==2.16.0
streamlit==1.29.0
```

## Deployment
The website is deployed using AWS EC2. The steps to deploy the website are as follows:
1. Launch an EC2 instance with the desired configuration.
2. SSH into the EC2 instance and install the required packages using `pip install -r requirements.txt`.
3. Run the streamlit app using `streamlit run app.py`.
4. Access the website using the public IP address of the EC2 instance.

## Website
![Alt Text](./imgs/Screenshot 2024-04-11 at 19.03.15.png)

![Alt Text](./imgs/Screenshot 2024-04-11 at 19.22.56.png)

![Alt Text](./imgs/Screenshot 2024-04-11 at 19.23.05.png)


